import { Component } from "react";
import { Navigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

// Used to protect routes.
// If user is null, in other words not logged in, navigates to empty path.
const withAuth = (Component) => (props) => {
  // hoc: higer order function = function that takes another function as input, or that returns a function
  const { user } = useUser();
  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to="/" />;
  }
};
export default withAuth;
