import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

// adds a new request to API
export const requestAdd = async (user, request) => {
  try {
    const response = await fetch(`${apiUrl}/${user.id}`, {
      method: "PATCH", // PATCH=update existing data, POST=add data, GET=read data
      headers: createHeaders(),
      body: JSON.stringify({
        username: user.username,
        translations: [...user.translations, request],
      }),
    });
    if (!response.ok) {
      throw new Error("Could not update request");
    }
    const result = await response.json();
    return [null, result];
  } catch (error) {
    return [error.message, null]; // null for the order
  }
};

export const requestClearHistory = (userId) => {};
