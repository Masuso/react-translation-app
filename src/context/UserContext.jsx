import { createContext, useContext, useState } from "react";
import { storageRead } from "../utils/storage";
import { STORAGE_KEY_USER } from "../consts/storageKeys"; // Used storage key to avoid magic strings

// context -> exposing
const UserContext = createContext();

export const useUser = () => {
  return useContext(UserContext); // {user, setUser}
};

// Provider -> managing state
const UserProvider = ({ children }) => {
  // redirects to profile page if state not null so that login is not shown again
  const [user, setUser] = useState(storageRead(STORAGE_KEY_USER));
  const state = {
    user,
    setUser,
  };

  return <UserContext.Provider value={state}>{children}</UserContext.Provider>;
};
export default UserProvider;
