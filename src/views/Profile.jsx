import { useEffect } from "react";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeaders from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { userById } from "../api/user";
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../consts/storageKeys";

// the profile page
const Profile = () => {
  const { user, setUser } = useUser(); // get access to the user
  useEffect(() => {
    // only runs on first render, not every re-render
    // useEffect can not be async
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id);
      if (error === null) {
        storageSave(STORAGE_KEY_USER, latestUser);
        setUser(latestUser);
      }
    };
    findUser();
  }, []);
  return (
    <>
      <h1>Profile</h1>
      <ProfileHeaders username={user.username} />
      <ProfileActions />
      <ProfileTranslationHistory translations={user.translations} />
    </>
  );
};

export default withAuth(Profile);
