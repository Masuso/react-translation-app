import { requestAdd } from "../api/requests";
import TranslationsForm from "../components/Translations/TranslationsForm";
import { STORAGE_KEY_USER } from "../consts/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

// the translation page
const Translations = () => {
  const { user, setUser } = useUser();

  // checks if translation request is valid
  const handleRequestSent = async (notes) => {
    if (!notes) {
      alert("Please write some text.");
      return;
    }
    if (!/^[a-zA-Z]+$/.test(notes)) {
      alert("Please only use latin characters.");
      return;
    }
    // send http request
    const [error, updatedUser] = await requestAdd(user, notes);
    if (error !== null) {
      return;
    }
    // keep UI state and Server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser);
    // update context state
    setUser(updatedUser);

    console.log("Error", error);
    console.log("updatedUser", updatedUser);
  };

  return (
    <>
      <h1>Translate</h1>
      <section id="translation-request">
        <TranslationsForm onRequest={handleRequestSent} />
      </section>
    </>
  );
};

export default withAuth(Translations);
