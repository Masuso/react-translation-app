import LoginForm from "../components/Login/LoginForm";

// the login page
const Login = () => {
  return (
    <>
      <h1>Login</h1>
      <LoginForm />
    </>
  );
};
export default Login;
