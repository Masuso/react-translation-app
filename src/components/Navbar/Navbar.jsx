import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

// creates navbar
const Navbar = () => {
  const { user } = useUser();

  return (
    <nav>
      <ul>
        <li>Translation requests</li>
      </ul>
      {user !== null && (
        <ul>
          <li>
            <NavLink to="/translations">Translations</NavLink>
          </li>
          <li>
            <NavLink to="/profile">Profile</NavLink>
          </li>
        </ul>
      )}
    </nav>
  );
};
export default Navbar;
