import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

// returns users' translation history
const ProfileTranslationHistory = ({ translations = [] }) => {
  const translationList = translations.map((translation, index) => (
    <ProfileTranslationHistoryItem
      key={index + "-" + translation} // '-' to avoid duplicate keys.
      translation={translation}
    />
  ));

  return (
    <section>
      <h4>Your translation history</h4>
      <ul>{translationList}</ul>
    </section>
  );
};
export default ProfileTranslationHistory;
