// makes header text for profile page
const ProfileHeader = ({ username }) => {
  return (
    <header>
      <h4>Hello, welcome back {username}</h4>
    </header>
  );
};
export default ProfileHeader;
