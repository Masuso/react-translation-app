// makes a list entry for a translation history item
const ProfileTranslationHistoryItem = ({ translation }) => {
  return <li>{translation}</li>;
};
export default ProfileTranslationHistoryItem;
