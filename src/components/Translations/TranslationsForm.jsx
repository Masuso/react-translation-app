import { useForm } from "react-hook-form";

// Makes form where user can type in translation requests
const TranslationsForm = ({ onRequest }) => {
  const { register, handleSubmit } = useForm();
  const onSubmit = ({ translationNotes }) => {
    onRequest(translationNotes);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset>
        <label htmlFor="translation-request">Translate:</label>
        <input
          type="text"
          {...register("translationNotes")}
          placeholder="Hello"
        ></input>
      </fieldset>
      <button type="submit">Translate</button>
    </form>
  );
};
export default TranslationsForm;
