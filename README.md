# Sign Language Translation App

This is supposed to be an app were the user can type in words, sentences or phrase using latin characters and translate them into sign language using pictures. The user can make a user and keep track of their translation history.
However the app is missing much of its planned functionality and can sadly not do much of anything.

## Deployment

To get the app running on your PC, clone or download the directory and run

### `npm start`

in the terminal from within the app directory to launch the app. Open [http://localhost:3000](http://localhost:3000) to view it in your browser. The page will reload when you make changes to the code.
To shut down the app, simply type 'ctrl + C' in the terminal.
